# Adapted from: https://learn.adafruit.com/edge-speech-recognition-with-voice2json/raspberry-pi-setup
#
# SPDX-FileCopyrightText: 2021 Melissa LeBlanc-Williams for Adafruit Industries
#
# SPDX-License-Identifier: MIT

import json
import os
import re
import signal
import subprocess
import time

import openflexure_microscope_client as ofm_client

wake_command = "/usr/bin/voice2json wait-wake --exit-count 1"
listen_command = (
    "/usr/bin/voice2json transcribe-stream | /usr/bin/voice2json recognize-intent"
)
transcribe_command = "/usr/bin/voice2json transcribe-stream"
recognize_command = "/usr/bin/voice2json recognize-intent"
speak_command = "/usr/bin/voice2json speak-sentence '{}'"
pattern = re.compile(r"(?<!^)(?=[A-Z])")

microscope = None
stop_listening = None


def speak(sentence):
    for output_line in shell_command(speak_command.format(sentence)):
        print(output_line, end="")


def transcribe_and_recognize():
    global stop_listening
    transcribe = subprocess.Popen(
        transcribe_command.split(), 
        stdout=subprocess.PIPE, 
        shell=False, 
        universal_newlines=True,
        preexec_fn=os.setsid,
    )
    print(f"transcribe PID: {transcribe.pid}")
    recognize = subprocess.Popen(
        recognize_command.split(), 
        stdout=subprocess.PIPE, 
        stdin=transcribe.stdout,
        shell=False,
        universal_newlines=True,
        preexec_fn=os.setsid,
    )
    print(f"recognize PID: {recognize.pid}")
    def kill_commands():
        print("killing process groups")
        for p in [transcribe, recognize]:
            os.killpg(os.getpgid(p.pid), signal.SIGTERM)
        print("waiting for processes to die")
        transcribe.wait()
        recognize.wait()
        print("processes are finished")

    stop_listening = kill_commands
    for stdout_line in iter(recognize.stdout.readline, ""):
        yield stdout_line
    recognize.stdout.close()
    for p in [recognize, transcribe]:
        return_code = p.wait()
        if return_code:
            e = subprocess.CalledProcessError(return_code, " ".join(p.args))
            print(f"Error in subprocess: {e}")
    stop_listening = None

def shell_command(cmd):
    popen = subprocess.Popen(
        cmd, stdout=subprocess.PIPE, shell=True, universal_newlines=True
    )
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)


def process_output(line):
    data = json.loads(line)
    if not data["timeout"] and data["intent"]["name"]:
        func_name = pattern.sub("_", data["intent"]["name"]).lower()
        if func_name in globals():
            globals()[func_name](**data["slots"])


def terminate_listening():
    global stop_listening
    speak("Stopping OpenFlexure voice control")
    stop_listening()


def autofocus():
    speak("Autofocussing")
    microscope.autofocus()


def capture_image():
    speak("Capturing image")
    microscope.capture_image()


def move_stage(axis, relative, steps):
    speak(f'Moving {axis} axis {"by" if relative else "to"} {steps} steps')
    if relative:
        microscope.move_rel(
            [
                steps if axis == "x" else 0,
                steps if axis == "y" else 0,
                steps if axis == "z" else 0,
            ]
        )
    else:
        microscope.move_rel(
            [
                steps if axis == "x" else 0,
                steps if axis == "y" else 0,
                steps if axis == "z" else 0,
            ]
        )


def move_stage_default(axis, direction):
    horizontal_steps = 200
    vertical_steps = 50
    sign = 1 if direction == "up" else -1
    speak(f"Moving {axis} axis {direction}")
    microscope.move_rel(
        [
            sign * horizontal_steps if axis == "x" else 0,
            sign * horizontal_steps if axis == "y" else 0,
            sign * vertical_steps if axis == "z" else 0,
        ]
    )


def get_position():
    position = microscope.position
    speak(f"Current position is:")
    speak(f"x = {position['x']}")
    speak(f"y = {position['y']}")
    speak(f"z = {position['z']}")


if __name__ == "__main__":
    while True:
        # Wait for wake command
        print("Waiting for wake command (Hey Mycroft)")
        [c for c in shell_command(wake_command)]
        speak("OpenFlexure voice control activated")
        # Connect to microscope
        try:
            microscope = ofm_client.MicroscopeClient("localhost")
        except:
            speak("Unable to connect to OpenFlexure device.")
            break
        # Listen (on loop) for commands and process
        for output_line in transcribe_and_recognize():
            process_output(output_line)

        time.sleep(1)
