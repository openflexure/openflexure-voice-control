#!/bin/bash
wget https://github.com/synesthesiam/voice2json/releases/download/v2.1/voice2json_2.1_armhf.deb
sudo apt --assume-yes install ./voice2json_2.1_armhf.deb
sudo apt --assume-yes install espeak-ng
voice2json -p en download-profile
cp sentences.ini ~/.local/share/voice2json/en-us_kaldi-zamia/sentences.ini
chmod +x ~/.local/share/voice2json/en-us_kaldi-zamia/slot_programs/rhasspy/number
voice2json train-profile
rm voice2json_2.1_armhf.deb
python3 -m venv .venv
source .venv/bin/activate
pip install openflexure-microscope-client
