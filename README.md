# OpenFlexure Voice Control

Voice control for the OpenFlexure Microscope, using [voice2JSON](http://voice2json.org/). Our paper describing our design decisions and how to use OpenFlexure Voice Control has been published in [Royal Society Open Science](https://doi.org/10.1098/rsos.221236).  

This project was initially inspired by the [adafruit 'Python Edge Speech Recognition with Voice2JSON' tutorial](https://learn.adafruit.com/edge-speech-recognition-with-voice2json).

## Setup

1. OpenFlexure Voice Control should be installed on the Raspberry Pi inside your OpenFlexure Microscope.  It has currently been tested on a Raspberry Pi 4 with 4GB RAM (but should currently also work on any `armhf` board). To find out whether your board is compatible, run in the terminal on your Raspberry Pi:

        `dpkg-architecture | grep DEB_BUILD_ARCH=`

2. Connect a microphone.  The easiest way to connect a microphone is to use a USB microphone. Most should be 'plug and play'. It has currently been tested using an [Amazon basics USB microphone](https://www.amazon.co.uk/AmazonBasics-Desktop-Mini-Condenser-Microphone-Black/dp/B076ZSR6BB?th=1).

3. Connect a speaker/headphones.  This is not strictly necessary to work but you will get audio feedback confirming commands. You could either use a bluetooth speaker/headphones, a USB speaker, or if you have connect your OpenFlexure Microscope to an HDMI monitor, the monitors speakers.

## Installation

1. Download or clone repository to your Raspberry Pi.
2. To install the program, on the terminal, run:  

        `./INSTALL.sh`


## Usage

1. In the terminal, run:

        `/RUN.sh`.

## Wake word

OpenFlexure Voice Control will not process any commands until you speak the wake-word:

> Hey, Mycroft

It will then keep listening for commands until told to stop. 

## Commands


These are some example commands you can use with OpenFlexure Voice Control. You also can try slight variations of these.

| Example command | Microscope's action |
|---|---|
| "Autofocus." | Run the fast autofocus routine. |
| "Capture image." | Capture an image. |
| "Move x axis by 100." | Move the stage in the x-direction 100 steps.|
| "Move the y axis to 0." | Move the stage to the x-axis absolute 0 position. |
| "Move z up." | Move the stage up 50 steps in the z-direction. |
| "What position are you?" | Speak the current position of the stage. |
| "Stop listening." | Stop actively listening until wake-word is heard. |


## Developing

1. If there are commands you would like added, please [raise an issue]() or make a [pull request]().
2.  OpenFlexure voice control is built around [voice2json](https://github.com/synesthesiam/voice2json) so I would recommend looking at their [documentation](http://voice2json.org/).
3.  There are two files that are relevant: `sentences.ini` and `openflexure-voice-control.py`.  

### `sentences.ini`

Here you create the individual commands.  I would recommend reading the [voice2json documentation](http://voice2json.org/sentences.html) on the voice command structure.  The important thing here is that the title of the python function should match the command name (converted to snake case).

### `openflexure-voice-control.py`

This is the script which uses `voice2json` to transcribe the voice command and determine the intent. Additional commands can be added here, using the [OpenFlexure Python Client](https://gitlab.com/openflexure/openflexure-microscope-pyclient).